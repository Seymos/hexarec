$(function () {

    /* Display message header */
    setTimeout(function () {
        $('#chat-notification').removeClass('hide').addClass('animated bounceIn');
        $('#chat-popup').removeClass('hide').addClass('animated fadeIn');
    }, 5000);

    /* Hide message header */
    setTimeout(function () {
        $('#chat-popup').removeClass('animated fadeIn').addClass('animated fadeOut').delay(800).hide(0);
    }, 8000);

    //****************** LINE & BAR SWITCH CHART ******************//
    var d1 = [
        [0, 950], [1, 1300], [2, 1600], [3, 1900], [4, 2100], [5, 2500], [6, 2200], [7, 2000], [8, 1950], [9, 1900], [10, 2000], [11, 2120]
    ];
    var d2 = [
        [0, 450], [1, 500], [2, 600], [3, 550], [4, 600], [5, 800], [6, 900], [7, 800], [8, 850], [9, 830], [10, 1000], [11, 1150]
    ];

    var tickArray = ['Janv', 'Fev', 'Mars', 'Apri', 'May', 'June', 'July', 'Augu', 'Sept', 'Nov'];

    /****  Line Chart  ****/
        var graph_lines = [{
        label: "Line 1",
        data: d1,
        lines: {
            lineWidth: 2
        },
        shadowSize: 0,
        color: '#3598DB'
    }, {
        label: "Line 1",
        data: d1,
        points: {
            show: true,
            fill: true,
            radius: 6,
            fillColor: "#3598DB",
            lineWidth: 3
        },
        color: '#fff'
    }, {
        label: "Line 2",
        data: d2,
        animator: {
            steps: 300,
            duration: 1000,
            start: 0
        },
        lines: {
            fill: 0.4,
            lineWidth: 0,
        },
        color: '#18a689'
    }, {
        label: "Line 2",
        data: d2,
        points: {
            show: true,
            fill: true,
            radius: 6,
            fillColor: "#99dbbb",
            lineWidth: 3
        },
        color: '#fff'
    } ];

    $('#graph-bars').hide();

    function showTooltip(x, y, contents) {
        $('<div id="flot-tooltip">' + contents + '</div>').css({
            position: 'absolute',
            display: 'none',
            top: y + 5,
            left: x + 5,
            color: '#fff',
            padding: '2px 5px',
            'background-color': '#717171',
            opacity: 0.80
        }).appendTo("body").fadeIn(200);
    }

    /* We get city from input on change */
    $("#city-form").change(function () {
        city = document.getElementById("city-form").value;
        $.simpleWeather({
            location: city,
            woeid: '',
            unit: 'f',
            success: function (weather) {
                city = weather.city;
                region = weather.country;
                tomorrow_date = weather.tomorrow.date;
                weather_icon = '<i class="icon-' + weather.code + '"></i>';
                $(".weather-city").html(city);
                $(".weather-currently").html(weather.currently);
                $(".today-img").html('<i class="big-img-weather icon-' + weather.code + '"></i>');
                $(".today-temp").html(weather.low + '° / ' + weather.high + '°');
                $(".weather-region").html(region);
                $(".weather-day").html(tomorrow_date);
                $(".weather-icon").html(weather_icon);
                $(".1-days-day").html(weather.forecasts.one.day);
                $(".1-days-image").html('<i class="icon-' + weather.forecasts.one.code + '"></i>');
                $(".1-days-temp").html(weather.forecasts.one.low + '° / ' + weather.forecasts.one.high + '°');
                $(".2-days-day").html(weather.forecasts.two.day);
                $(".2-days-image").html('<i class="icon-' + weather.forecasts.two.code + '"></i>');
                $(".2-days-temp").html(weather.forecasts.two.low + '° / ' + weather.forecasts.two.high + '°');
                $(".3-days-day").html(weather.forecasts.three.day);
                $(".3-days-image").html('<i class="icon-' + weather.forecasts.three.code + '"></i>');
                $(".3-days-temp").html(weather.forecasts.three.low + '° / ' + weather.forecasts.three.high + '°');
                $(".4-days-day").html(weather.forecasts.four.day);
                $(".4-days-image").html('<i class="icon-' + weather.forecasts.four.code + '"></i>');
                $(".4-days-temp").html(weather.forecasts.four.low + '° / ' + weather.forecasts.four.high + '°');
            }
        });
    });

});