<?php
namespace Hexarec\AdminBundle\Controller;

use Hexarec\AdminBundle\Form\ArtistCategoryType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Hexarec\AdminBundle\Entity\ArtistCategory;
use Symfony\Component\HttpFoundation\Request;

class ArtistCategoryController extends Controller
{
    public function allAction()
    {
        $repository = $this->getDoctrine()->getRepository('AdminBundle:ArtistCategory');
        $categories = $repository->findAll();

        return $this->render(
            'AdminBundle:ArtistCategory:all.html.twig',
            array(
                'categories' => $categories
            )
        );
    }

    public function addAction(Request $request) {
        $category = new ArtistCategory();
        $form = $this->createForm(ArtistCategoryType::class, $category);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();
            return $this->redirectToRoute('artist_category_all');
        }
        return $this->render(
            'AdminBundle:ArtistCategory:add.html.twig',
            array(
                'form' => $form->createView()
            )
        );
    }
    public function showAction($id)
    {
        $repository = $this->getDoctrine()->getRepository('AdminBundle:ArtistCategory');
        $category = $repository->find($id);

        return $this->render(
            'AdminBundle:ArtistCategory:show.html.twig',
            array(
                'category' => $category
            )
        );
    }
    public function editAction(Request $request, $id)
    {
        $repository = $this->getDoctrine()->getRepository('AdminBundle:ArtistCategory');
        $category = $repository->find($id);
        $form = $this->createForm(ArtistCategoryType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();
            return $this->redirectToRoute('artist_category_all');
        }
        return $this->render(
            'AdminBundle:ArtistCategory:edit.html.twig',
            array(
                'form' => $form->createView(),
                'category' => $category
            )
        );
    }
    public function removeAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $category = $em->getRepository('AdminBundle:ArtistCategory')->find($id);
        $em->remove($category);
        $em->flush();

        return $this->redirectToRoute('artist_category_all');
    }
}