<?php

namespace Hexarec\AdminBundle\Controller;

use Hexarec\AdminBundle\Form\MediaType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Hexarec\AdminBundle\Entity\Media;
use Symfony\Component\HttpFoundation\Request;

class MediaController extends Controller
{
    public function allAction()
    {
        $em = $this->getDoctrine()->getManager();
        $medias = $em->getRepository('AdminBundle:Media')->findAll();
        $categories = $em->getRepository('AdminBundle:MediaCategory')->findAll();

        return $this->render(
            'AdminBundle:Media:all.html.twig',
            array(
                'medias' => $medias,
                'categories' => $categories
            )
        );
    }

    public function addAction(Request $request) {
        $media = new Media();
        $form = $this->createForm(MediaType::class, $media);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $image = $media->getImage();
            $fileName = $this->get('app.medias_thumb_directory')->upload($image);

            $media->setImage($fileName);
            $em = $this->getDoctrine()->getManager();
            $em->persist($media);
            $em->flush();
            return $this->redirectToRoute('media_all');
        }
        return $this->render(
            'AdminBundle:Media:add.html.twig',
            array(
                'form' => $form->createView()
            )
        );
    }
    public function showAction($id)
    {
        $repository = $this->getDoctrine()->getRepository('AdminBundle:Media');
        $media = $repository->find($id);

        return $this->render(
            'AdminBundle:Media:show.html.twig',
            array(
                'media' => $media
            )
        );
    }
    public function editAction(Request $request, $id)
    {
        $repository = $this->getDoctrine()->getRepository('AdminBundle:Media');
        $media = $repository->find($id);
        $form = $this->createForm(MediaType::class, $media);
        $default = $media->getImage();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($form->getData()->getImage() !== null) {
                $image = $media->getImage();
                $fileName = $this->get('app.medias_thumb_directory')->upload($image);
                $media->setImage($fileName);
            } else {
                $media->setImage($default);
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($media);
            $em->flush();
            return $this->redirectToRoute('media_all');
        }
        return $this->render(
            'AdminBundle:Media:edit.html.twig',
            array(
                'form' => $form->createView(),
                'media' => $media
            )
        );
    }
    public function removeAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $artist = $em->getRepository('AdminBundle:Media')->find($id);
        $em->remove($artist);
        $em->flush();

        return $this->redirectToRoute('media_all');
    }
}
