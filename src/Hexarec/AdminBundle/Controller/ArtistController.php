<?php

namespace Hexarec\AdminBundle\Controller;

use Hexarec\AdminBundle\Form\ArtistType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Hexarec\AdminBundle\Entity\Artist;
use Symfony\Component\HttpFoundation\Request;

class ArtistController extends Controller
{
    public function allAction()
    {
        $em = $this->getDoctrine()->getManager();
        //$repository = $this->getDoctrine()->getRepository('AdminBundle:Artist');
        $artists = $em->getRepository('AdminBundle:Artist')->findAll();
        $remixers = $em->getRepository('AdminBundle:Artist')->getArtists("2");

        return $this->render(
            'AdminBundle:Artist:all.html.twig',
            array(
                'artists' => $artists,
                'remixers' => $remixers
            )
        );
    }

    public function addAction(Request $request) {
        $artist = new Artist();
        $form = $this->createForm(ArtistType::class, $artist);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $image = $artist->getImage();
            $fileName = $this->get('app.artist_uploader')->upload($image);

            $artist->setImage($fileName);
            $em = $this->getDoctrine()->getManager();
            $em->persist($artist);
            $em->flush();
            return $this->redirectToRoute('artist_all');
        }
        return $this->render(
            'AdminBundle:Artist:add.html.twig',
            array(
                'form' => $form->createView()
            )
        );
    }
    public function showAction($id)
    {
        $repository = $this->getDoctrine()->getRepository('AdminBundle:Artist');
        $artist = $repository->find($id);
        $albums = $artist->getAlbums();
        return $this->render(
            'AdminBundle:Artist:show.html.twig',
            array(
                'artist' => $artist,
                'albums' => $albums
            )
        );
    }
    public function editAction(Request $request, $id)
    {
        $repository = $this->getDoctrine()->getRepository('AdminBundle:Artist');
        $artist = $repository->find($id);
        $default = $artist->getImage();

        $form = $this->createForm(ArtistType::class, $artist);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($form->getData()->getImage() !== null) {
                $image = $artist->getImage();
                $fileName = $this->get('app.artist_uploader')->upload($image);
                $artist->setImage($fileName);
            } else {
                $artist->setImage($default);
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($artist);
            $em->flush();
            return $this->redirectToRoute('artist_all');
        }
        return $this->render(
            'AdminBundle:Artist:edit.html.twig',
            array(
                'form' => $form->createView(),
                'artist' => $artist
            )
        );
    }
    public function removeAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $artist = $em->getRepository('AdminBundle:Artist')->find($id);
        $em->remove($artist);
        $em->flush();

        return $this->redirectToRoute('artist_all');
    }
}
