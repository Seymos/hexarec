<?php
namespace Hexarec\AdminBundle\Controller;

use Hexarec\AdminBundle\Form\MediaCategoryType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Hexarec\AdminBundle\Entity\MediaCategory;
use Symfony\Component\HttpFoundation\Request;

class MediaCategoryController extends Controller
{
    public function allAction()
    {
        $repository = $this->getDoctrine()->getRepository('AdminBundle:MediaCategory');
        $categories = $repository->findAll();

        return $this->render(
            'AdminBundle:MediaCategory:all.html.twig',
            array(
                'categories' => $categories
            )
        );
    }

    public function addAction(Request $request) {
        $category = new MediaCategory();
        $form = $this->createForm(MediaCategoryType::class, $category);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();
            return $this->redirectToRoute('media_category_all');
        }
        return $this->render(
            'AdminBundle:MediaCategory:add.html.twig',
            array(
                'form' => $form->createView()
            )
        );
    }
    public function showAction($id)
    {
        $repository = $this->getDoctrine()->getRepository('AdminBundle:MediaCategory');
        $category = $repository->find($id);

        return $this->render(
            'AdminBundle:MediaCategory:show.html.twig',
            array(
                'category' => $category
            )
        );
    }
    public function editAction(Request $request, $id)
    {
        $repository = $this->getDoctrine()->getRepository('AdminBundle:MediaCategory');
        $category = $repository->find($id);
        $form = $this->createForm(MediaCategoryType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();
            return $this->redirectToRoute('media_category_all');
        }
        return $this->render(
            'AdminBundle:MediaCategory:edit.html.twig',
            array(
                'form' => $form->createView(),
                'category' => $category
            )
        );
    }
    public function removeAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $category = $em->getRepository('AdminBundle:MediaCategory')->find($id);
        $em->remove($category);
        $em->flush();

        return $this->redirectToRoute('media_category_all');
    }
}