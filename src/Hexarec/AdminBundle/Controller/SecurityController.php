<?php

namespace Hexarec\AdminBundle\Controller;

use Admin\AdminBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Admin\AdminBundle\Form\UserForm;
use FOS\UserBundle\Form\Type\RegistrationFormType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\FOSUserEvents;

class SecurityController extends Controller
{
    /**
     * @Security("has_role('ROLE_USER')")
     */
    public function usersAction() {
        $repository = $this->getDoctrine()->getRepository('AdminBundle:User');
        $users = $repository->findAll();

        return $this->render(
            'AdminBundle:User:all.html.twig',
            array(
                'users' => $users
            )
        );
    }

    public function userEditAction(Request $request, $id) {
        $repository = $this->getDoctrine()->getRepository('AdminBundle:User');
        $user = $repository->find($id);

        $form = $this->createForm(UserForm::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('user_all');
        }

        return $this->render(
            'AdminBundle:User:userEdit.html.twig',
            array(
                'user' => $user,
                'form' => $form->createView()
            )
        );
    }

    public function userRemoveAction($id) {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('AdminBundle:User')->find($id);
        $em->remove($user);
        $em->flush();

        return $this->redirectToRoute('user_all');
    }
}