<?php

namespace Hexarec\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class UserController extends Controller
{
    public function getUserAction()
    {
        $user = $this->getUser();
        return $this->render(
            'AdminBundle:User:user.html.twig',
            array(
                'user' => $user
            )
        );
    }

    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $users = $em->getRepository('AdminBundle:User')->findAll();

        return $this->render(
            'AdminBundle:User:index.html.twig',
            array(
                'users' => $users
            )
        );
    }

    public function removeAction($id) {
        $em = $this->getDoctrine()->getManager();

        $user = $em->getRepository('AdminBundle:User')->find($id);
        $em->remove($user);
        $em->flush();

        return $this->redirectToRoute('admin_users');
    }
}