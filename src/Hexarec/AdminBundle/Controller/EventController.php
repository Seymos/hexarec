<?php

namespace Hexarec\AdminBundle\Controller;

use Hexarec\AdminBundle\Form\EventType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Hexarec\AdminBundle\Entity\Event;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class EventController extends Controller
{
    public function allAction()
    {
        $repository = $this->getDoctrine()->getRepository('AdminBundle:Event');
        $events = $repository->findAll();

        return $this->render(
            'AdminBundle:Event:all.html.twig',
            array(
                'events' => $events
            )
        );
    }
    public function addAction(Request $request)
    {
        $event = new Event();
        $form = $this->createForm(EventType::class, $event);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $image = $event->getImage();
            $fileName = $this->get('app.event_uploader')->upload($image);
            $event->setImage($fileName);
            $em = $this->getDoctrine()->getManager();
            $em->persist($event);
            $em->flush();

            return $this->redirectToRoute('event_all');
        }

        return $this->render(
            'AdminBundle:Event:add.html.twig',
            array(
                'form' => $form->createView()
            )
        );
    }
    public function showAction($id)
    {
        $repository = $this->getDoctrine()->getRepository('AdminBundle:Event');
        $event = $repository->find($id);

        return $this->render(
            'AdminBundle:Event:show.html.twig',
            array(
                'event' => $event
            )
        );
    }
    public function editAction(Request $request, $id)
    {
        $repository = $this->getDoctrine()->getRepository('AdminBundle:Event');
        $event = $repository->find($id);
        $illustration = $event->getImage();

        $form = $this->createForm(EventType::class, $event);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($form->getData()->getImage() !== null) {
                $image = $event->getImage();
                $fileName = $this->get('app.event_uploader')->upload($image);
                $event->setImage($fileName);
            } else {
                $event->setImage($illustration);
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($event);
            $em->flush();

            return $this->redirectToRoute('event_show', array('id' => $event->getId()));
        }
        return $this->render(
            'AdminBundle:Event:edit.html.twig',
            array(
                'form' => $form->createView(),
                'event' => $event
            )
        );
    }
    public function removeAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $event = $em->getRepository('AdminBundle:Event')->find($id);
        $em->remove($event);
        $em->flush();
        return $this->redirectToRoute('event_all');
    }
}
