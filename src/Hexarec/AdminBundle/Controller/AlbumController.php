<?php

namespace Hexarec\AdminBundle\Controller;

use Hexarec\AdminBundle\Entity\Album;
use Hexarec\AdminBundle\Entity\Artist;
use Hexarec\AdminBundle\Form\AlbumType;
use Hexarec\AdminBundle\Form\ArtistsType;
use Hexarec\AdminBundle\Form\ArtistType;
use Hexarec\AdminBundle\Form\RemixerType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;

class AlbumController extends Controller
{
    public function allAction()
    {
        $repository = $this->getDoctrine()->getRepository('AdminBundle:Album');
        $albums = $repository->findAll();
        return $this->render(
            'AdminBundle:Discography:all.html.twig',
            array(
                'albums' => $albums
            )
        );
    }
    public function addAction(Request $request)
    {
        $album = new Album();
        $form = $this->createForm(AlbumType::class, $album);
        $artists = $this->getDoctrine()->getRepository('AdminBundle:Artist')->findAll();
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $image = $album->getImage();
            $fileName = $this->get('app.album_uploader')->upload($image);

            $album->setImage($fileName);
            $em = $this->getDoctrine()->getManager();
            $em->persist($album);
            $em->flush();

            return $this->redirectToRoute('album_all');
        }
        return $this->render(
            'AdminBundle:Discography:add.html.twig',
            array(
                'form' => $form->createView(),
                'artists' => $artists
            )
        );
    }
    public function showAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $album = $em->getRepository('AdminBundle:Album')->find($id);
        $remixers = $em->getRepository('AdminBundle:Artist')->findAll();

        if ($request->isMethod('POST')) {
            $remixer = $em->getRepository('AdminBundle:Artist')->findOneBy(
                array('id' => $request->get('remixers'))
            );
            $album->addArtist($remixer);
            $em->persist($album);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success',"Le remixer a correctement été ajouté à l'album.");
            return $this->redirectToRoute('album_show', array('id' => $album->getId()));
        }

        return $this->render(
            'AdminBundle:Discography:show.html.twig',
            array(
                'album' => $album,
                'remixers' => $remixers
            )
        );
    }
    public function removeRmxAction($id, $slug) {
        $em = $this->getDoctrine()->getManager();
        $album = $em->getRepository('AdminBundle:Album')->find($id);
        $remixer = $em->getRepository('AdminBundle:Artist')->findOneBy(
            array('slug' => $slug)
        );
        $album->removeArtist($remixer);
        $em->persist($album);
        $em->flush();
        $this->get('session')->getFlashBag()->add('success',"Remixer supprimé de cet album.");
        return $this->redirectToRoute('album_show', array('id' => $album->getId()));
    }
    public function editAction(Request $request, $id)
    {
        $repository = $this->getDoctrine()->getRepository('AdminBundle:Album');
        $artists = $this->getDoctrine()->getRepository('AdminBundle:Artist')->findAll();
        $album = $repository->find($id);
        $previousImage = $album->getImage();

        $form = $this->createForm(AlbumType::class, $album);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            if ($form->getData()->getImage() !== null) {
                $image = $album->getImage();
                $fileName = $this->get('app.album_uploader')->upload($image);
                $album->setImage($fileName);
            } else {
                $album->setImage($previousImage);
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($album);
            $em->flush();

            return $this->redirectToRoute('album_show', array('id' => $album->getId()));
        }
        return $this->render(
            'AdminBundle:Discography:edit.html.twig',
            array(
                'album' => $album,
                'form' => $form->createView(),
                'artists' => $artists
            )
        );
    }
    public function removeAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $album = $em->getRepository('AdminBundle:Album')->find($id);
        $em->remove($album);
        $em->flush();

        return $this->redirectToRoute('album_all');
    }
}
