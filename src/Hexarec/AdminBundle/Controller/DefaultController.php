<?php

namespace Hexarec\AdminBundle\Controller;

use FOS\UserBundle\Model\UserInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $user = $this->getUser();
            if (is_object($user) && $user instanceof UserInterface) {
                if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
                    $em = $this->getDoctrine()->getManager();
                    $albums = $em->getRepository('AdminBundle:Album')->findBy(
                        array(),
                        array('id' => 'DESC'),
                        6,
                        0
                    );
                    $medias = $em->getRepository('AdminBundle:Media')->findBy(
                        array(),
                        array('id' => 'DESC'),
                        4,
                        0
                    );
                    $artists = $em->getRepository('AdminBundle:Artist')->findBy(
                        array(),
                        array('id' => 'DESC'),
                        3,
                        0
                    );
                    /*$songs = $em->getRepository('AdminBundle:Album')->findBy(
                        array(),
                        array('id' => 'DESC'),
                        6,
                        0
                    );*/
                    return $this->render(
                        'AdminBundle:Default:index.html.twig',
                        array(
                            'albums' => $albums,
                            'medias' => $medias,
                            'artists' => $artists
                        )
                    );
                }
            }
        }
        return $this->redirectToRoute('homepage');
    }
}
