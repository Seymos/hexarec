<?php

namespace Hexarec\AdminBundle\Controller;

use Hexarec\AdminBundle\Entity\Article;
use Hexarec\AdminBundle\Form\ArticleType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

class ArticleController extends Controller
{
    public function allAction()
    {
        $repository = $this->getDoctrine()->getRepository('AdminBundle:Article');
        $articles = $repository->findAll();

        return $this->render(
            'AdminBundle:Article:all.html.twig',
            array(
                'articles' => $articles
            )
        );
    }

    public function showAction($id)
    {
        $repository = $this->getDoctrine()->getRepository('AdminBundle:Article');
        $article = $repository->find($id);

        return $this->render(
            'AdminBundle:Article:show.html.twig',
            array(
                'article' => $article
            )
        );
    }

    public function addAction(Request $request)
    {
        $article = new Article();
        $user = $this->getUser();
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $image = $article->getImage();
            $fileName = $this->get('app.article_uploader')->upload($image);
            $article->setImage($fileName);

            $em = $this->getDoctrine()->getManager();
            $em->persist($article);
            $em->flush();

            return $this->redirectToRoute('article_all');
        }
        return $this->render(
            'AdminBundle:Article:add.html.twig',
            array(
                'form' => $form->createView()
            )
        );
    }
    public function editAction(Request $request, $id)
    {
        $repository = $this->getDoctrine()->getRepository('AdminBundle:Article');
        $article = $repository->find($id);
        $previousImage = $article->getImage();
        $user = $this->getUser();
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($form->getData()->getImage() !== null) {
                /** @var UploadedFile $file */
                $file = $article->getImage();
                /** @var UploadedFile $fileName */
                $fileName = strtolower(str_replace(" ", "_", $article->getTitle())).'.'.$file->guessExtension();

                $file->move(
                    $this->getParameter('article_directory'),
                    $fileName
                );
                $article->setImage($fileName);
            } else {
                $article->setImage($previousImage);
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($article);
            $em->flush();

            return $this->redirectToRoute('article_show', array('id' => $article->getId()));
        }
        return $this->render(
            'AdminBundle:Article:edit.html.twig',
            array(
                'form' => $form->createView(),
                'article' => $article,
                'user' => $user
            )
        );
    }
    public function removeAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $article = $em->getRepository('AdminBundle:Article')->find($id);
        $em->remove($article);
        $em->flush();
        return $this->redirectToRoute('article_all');
    }
}
