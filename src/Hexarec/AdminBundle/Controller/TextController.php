<?php
namespace Hexarec\AdminBundle\Controller;

use Hexarec\AdminBundle\Entity\Text;
use Hexarec\AdminBundle\Form\TextForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class TextController extends Controller
{
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();
        $texts = $em->getRepository('AdminBundle:Text')->findAll();
        return $this->render(
            'AdminBundle:Text:index.html.twig',
            array(
                'texts' => $texts
            )
        );
    }

    public function createAction(Request $request) {
        $text = new Text();
        $form = $this->createForm(TextForm::class, $text);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($text);
            $em->flush();
            return $this->redirectToRoute('admin_text_index');
        }
        return $this->render(
            'AdminBundle:Text:create.html.twig',
            array(
                'form' => $form->createView()
            )
        );
    }

    public function editAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $text = $em->getRepository('AdminBundle:Text')->find($id);
        $form = $this->createForm(TextForm::class, $text);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($text);
            $em->flush();
            return $this->redirectToRoute('admin_text_index');
        }
        return $this->render(
            'AdminBundle:Text:edit.html.twig',
            array(
                'form' => $form->createView(),
                'text' => $text
            )
        );
    }
}