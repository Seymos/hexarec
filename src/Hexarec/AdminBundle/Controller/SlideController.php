<?php
namespace Hexarec\AdminBundle\Controller;

use Hexarec\AdminBundle\Entity\Slide;
use Hexarec\AdminBundle\Form\SlideType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class SlideController extends Controller
{
    public function indexAction() {
        $repository = $this->getDoctrine()->getRepository('AdminBundle:Slide');
        $slides = $repository->findAll();
        return $this->render(
            'AdminBundle:Slide:index.html.twig',
            array(
                'slides' => $slides
            )
        );
    }

    public function addAction(Request $request)
    {
        $slide = new Slide();
        $form = $this->createForm(SlideType::class, $slide);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            if($form->getData()->getAlbum() === null) {
                $media = $form->getData()->getMedia();
                $slide->setMedia($media);
            } else {
                $album = $form->getData()->getAlbum();
                $slide->setAlbum($album);
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($slide);
            $em->flush();

            return $this->redirectToRoute('admin_slider_index');
        }
        return $this->render(
            'AdminBundle:Slide:add.html.twig',
            array(
                'form' => $form->createView()
            )
        );
    }

    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();
        $slider = $this->getDoctrine()->getRepository('AdminBundle:Slide')->find($id);
        if ($slider->getAlbum() != null) {
            $album_id = $slider->getAlbum()->getId();
            $slide = $em->getRepository('AdminBundle:Album')->findOneBy(
                array('id' => $album_id)
            );
            return $this->render(
                'AdminBundle:Slide:album.html.twig',
                array(
                    'slide' => $slide
                )
            );
        } else {
            $media_id = $slider->getMedia()->getId();
            $slide = $em->getRepository('AdminBundle:Media')->find($media_id);
            return $this->render(
                'AdminBundle:Slide:media.html.twig',
                array(
                    'slide' => $slide
                )
            );
        }
    }

    public function editAction(Request $request, $id)
    {
        $repository = $this->getDoctrine()->getRepository('AdminBundle:Slide');
        $slide = $repository->find($id);

        $form = $this->createForm(SlideType::class, $slide);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($slide);
            $em->flush();

            return $this->redirectToRoute('admin_slider_index');
        }
        return $this->render(
            'AdminBundle:Slide:edit.html.twig',
            array(
                'slide' => $slide,
                'form' => $form->createView()
            )
        );
    }

    public function removeAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $slide = $em->getRepository('AdminBundle:Slide')->find($id);
        $em->remove($slide);
        $em->flush();

        return $this->redirectToRoute('admin_slider_index');
    }
}