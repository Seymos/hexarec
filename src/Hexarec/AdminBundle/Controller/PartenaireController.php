<?php
namespace Hexarec\AdminBundle\Controller;

use Hexarec\AdminBundle\Entity\Partenaire;
use Hexarec\AdminBundle\Form\PartenaireType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class PartenaireController extends Controller
{
    public function allAction() {
        $em = $this->getDoctrine()->getManager();
        $partenaires = $em->getRepository('AdminBundle:Partenaire')->findAll();
        return $this->render(
            'AdminBundle:Partenaire:all.html.twig',
            array(
                'partenaires' => $partenaires
            )
        );
    }

    public function addAction(Request $request) {
        $partenaire = new Partenaire();
        $form = $this->createForm(PartenaireType::class, $partenaire);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $image = $partenaire->getImage();
            $fileName = $this->get('app.partenaire_uploader')->upload($image);

            $partenaire->setImage($fileName);
            $em = $this->getDoctrine()->getManager();
            $em->persist($partenaire);
            $em->flush();
            return $this->redirectToRoute('partenaire_all');
        }
        return $this->render(
            'AdminBundle:Partenaire:add.html.twig',
            array(
                'form' => $form->createView()
            )
        );
    }

    public function editAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $partenaire = $em->getRepository('AdminBundle:Partenaire')->find($id);
        $previousImage = $partenaire->getImage();
        $form = $this->createForm(PartenaireType::class, $partenaire);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($form->getData()->getImage() != null) {
                $image = $partenaire->getImage();
                $fileName = $this->get('app.partenaire_uploader')->upload($image);

                $partenaire->setImage($fileName);
            } else {
                $partenaire->setImage($previousImage);
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($partenaire);
            $em->flush();
            return $this->redirectToRoute('partenaire_all');
        }
        return $this->render(
            'AdminBundle:Partenaire:edit.html.twig',
            array(
                'form' => $form->createView(),
                'partenaire' => $partenaire
            )
        );
    }

    public function removeAction($id) {
        $em = $this->getDoctrine()->getManager();
        $partenaire = $em->getRepository('AdminBundle:Partenaire')->find($id);
        if ($partenaire != null) {
            $em->remove($partenaire);
        }
        return $this->redirectToRoute('partenaire_all');
    }
}