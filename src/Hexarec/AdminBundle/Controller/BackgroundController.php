<?php
namespace Hexarec\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Hexarec\AdminBundle\Entity\Background;
use Hexarec\AdminBundle\Form\BackgroundType;
use Symfony\Component\HttpFoundation\Request;

class BackgroundController extends Controller
{
    public function indexAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $backgrounds = $em->getRepository('AdminBundle:Background')->findBy(
            array(),
            array('id' => 'asc')
        );

        $background = new Background();
        $form = $this->createForm(BackgroundType::class, $background);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $image = $background->getImage();
            $fileName = $this->get('app.background_uploader')->upload($image);

            $background->setImage($fileName);
            $background->setActive(0);
            $em = $this->getDoctrine()->getManager();
            $em->persist($background);
            $em->flush();

            return $this->redirectToRoute('background_index');
        }
        return $this->render(
            'AdminBundle:Background:index.html.twig',
            array(
                'form' => $form->createView(),
                'backgrounds' => $backgrounds
            )
        );
    }

    public function changeAction($id) {
        $em = $this->getDoctrine()->getManager();
        $backgrounds = $em->getRepository('AdminBundle:Background')->findAll();
        foreach ($backgrounds as $background) {
            $background->setActive(0);
            $em->flush();
        }
        $background = $em->getRepository('AdminBundle:Background')->find($id);
        $background->setActive(1);
        $em->flush();
        $value = $background->getImage();

        return $this->redirectToRoute(
            'background_index',
            array(
                'value' => $value
            )
        );
    }
}