<?php

namespace Hexarec\AdminBundle\Controller;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use Hexarec\AdminBundle\Entity\Label;
use Hexarec\AdminBundle\Form\LabelForm;

class LabelController extends Controller
{
    public function allAction() {
        $repository = $this->getDoctrine()->getRepository('AdminBundle:Label');
        $labels = $repository->findAll();

        return $this->render(
            'AdminBundle:Label:all.html.twig',
            array(
                'labels' => $labels
            )
        );
    }

    public function addAction(Request $request) {
        $label = new Label();

        $form = $this->createFormBuilder($label)
            ->add('title', TextType::class)
            ->add('submit', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($label);
            $em->flush();
            return $this->redirectToRoute('label_all');
        }
        return $this->render(
            'AdminBundle:Label:add.html.twig',
            array(
                'form' => $form->createView()
            )
        );
    }

    public function editAction(Request $request, $id) {
        $repository = $this->getDoctrine()->getRepository('AdminBundle:Label');
        $label = $repository->find($id);
        $form = $this->createFormBuilder($label)
            ->add('title', TextType::class)
            ->add('submit', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($label);
            $em->flush();
            return $this->redirectToRoute('label_all');
        }
        return $this->render(
            'AdminBundle:Label:edit.html.twig',
            array(
                'form' => $form->createView(),
                'label' => $label
            )
        );
    }

    public function removeAction($id) {
        $em = $this->getDoctrine()->getManager();
        $label = $em->getRepository('AdminBundle:Label')->find($id);
        $em->remove($label);
        $em->flush();

        return $this->redirectToRoute('label_all');
    }
}