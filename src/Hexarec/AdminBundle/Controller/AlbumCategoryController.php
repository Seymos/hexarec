<?php
namespace Hexarec\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Hexarec\AdminBundle\Form\AlbumCategoryType;
use Hexarec\AdminBundle\Entity\AlbumCategory;
use Symfony\Component\HttpFoundation\Request;

class AlbumCategoryController extends Controller 
{
    public function allAction()
    {
        $repository = $this->getDoctrine()->getRepository('AdminBundle:AlbumCategory');
        $categories = $repository->findAll();

        return $this->render(
            'AdminBundle:AlbumCategory:all.html.twig',
            array(
                'categories' => $categories
            )
        );
    }

    public function addAction(Request $request) {
        $category = new AlbumCategory();
        $form = $this->createForm(AlbumCategoryType::class, $category);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();
            return $this->redirectToRoute('album_category_all');
        }
        return $this->render(
            'AdminBundle:AlbumCategory:add.html.twig',
            array(
                'form' => $form->createView()
            )
        );
    }
    public function showAction($id)
    {
        $repository = $this->getDoctrine()->getRepository('AdminBundle:AlbumCategory');
        $category = $repository->find($id);

        return $this->render(
            'AdminBundle:AlbumCategory:show.html.twig',
            array(
                'category' => $category
            )
        );
    }
    public function editAction(Request $request, $id)
    {
        $repository = $this->getDoctrine()->getRepository('AdminBundle:AlbumCategory');
        $category = $repository->find($id);
        $form = $this->createForm(AlbumCategoryType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();
            return $this->redirectToRoute('album_category_all');
        }
        return $this->render(
            'AdminBundle:AlbumCategory:edit.html.twig',
            array(
                'form' => $form->createView(),
                'category' => $category
            )
        );
    }
    public function removeAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $category = $em->getRepository('AdminBundle:AlbumCategory')->find($id);
        $em->remove($category);
        $em->flush();

        return $this->redirectToRoute('album_category_all');
    }
}