<?php
namespace Hexarec\AdminBundle\Twig\Extension;

class SummaryExtension extends \Twig_Extension
{
    public function getFilters() {
        return array(
            new \Twig_SimpleFilter(
                'summary', array($this,'summary')
            )
        );
    }

    public function summary($str) {
        return substr($str, 0, 5);
    }

    public function getName()
    {
        return 'summary_extension';
    }
}