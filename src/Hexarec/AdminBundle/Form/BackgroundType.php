<?php

namespace Hexarec\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BackgroundType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'image',
                FileType::class,
                array(
                    'label' => 'Choisissez votre fond de site',
                    'data_class' => null,
                    'required' => false
                )
            )
            ->add(
                'save',
                SubmitType::class,
                array(
                    'label' => 'Sauvegarder',
                    'attr' => array(
                        'class' => 'btn btn-primary'
                    )
                )
            )
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Hexarec\AdminBundle\Entity\Background'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'hexarec_adminbundle_background';
    }


}
