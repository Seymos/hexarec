<?php

namespace Hexarec\AdminBundle\Form;

use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MediaType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'title',
                TextType::class,
                array(
                    'attr' => array(
                        'class' => 'form-control'
                    )
                )
            )
            ->add(
                'slug',
                HiddenType::class,
                array(
                    'attr' => array(
                        'class' => 'form-control'
                    )
                )
            )
            ->add(
                'image',
                FileType::class,
                array(
                    'label' => 'Image (obligatoire)',
                    'data_class' => null,
                    'required' => true
                )
            )
            ->add(
                'content',
                CKEditorType::class,
                array(
                    'label' => "Description et contenu (facultatif)",
                    'config' => array(
                        'toolbar' => 'full',
                        'uiColor' => '#ffffff',
                    ),
                )
            )
            ->add(
                'url',
                UrlType::class,
                array(
                    'label' => "Url de la vidéo (facultatif)",
                    'required' => false
                )
            )
            ->add(
                'code',
                TextareaType::class,
                array(
                    'label' => "Code d'intégration de la vidéo (YT, etc) (facultatif)",
                    'required' => false
                )
            )
            ->add(
                'datePublication',
                HiddenType::class,
                array(
                    'attr' => array(
                        'class' => 'form-control'
                    ),
                    'required' => false,
                )
            )
            ->add(
                'category',
                EntityType::class,
                array(
                    'class' => "AdminBundle:MediaCategory",
                    'choice_label' => "name",
                    'label' => "Catégorie média",
                    'attr' => array(
                        'class' => 'form-control'
                    )
                )
            )
            ->add(
                'save',
                SubmitType::class,
                array(
                    'label' => "Sauvegarder",
                    'attr' => array(
                        'class' => 'form-control'
                    )
                )
            )
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Hexarec\AdminBundle\Entity\Media'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'hexarec_adminbundle_media';
    }


}
