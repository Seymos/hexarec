<?php

namespace Hexarec\AdminBundle\Form;

use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ArtistType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'name',
                TextType::class,
                array(
                    'attr' => array(
                        'class' => 'form-control'
                    )
                )
            )
            ->add(
                'facebook',
                UrlType::class,
                array(
                    'required' => false,
                    'attr' => array(
                        'class' => 'form-control'
                    )
                )
            )
            ->add(
                'slug',
                TextType::class,
                array(
                    'attr' => array(
                        'class' => 'form-control'
                    )
                )
            )
            ->add(
                'image',
                FileType::class,
                array(
                    'label' => 'Avatar',
                    'data_class' => null,
                    'required' => false
                )
            )
            ->add(
                'content',
                CKEditorType::class,
                array(
                    'required' => false,
                    'label' => "Description et contenu",
                    'attr' => array(
                        'async' => false
                    ),
                    'config' => array(
                        'toolbar' => 'full',
                        'uiColor' => '#ffffff',
                    ),
                )
            )
            ->add(
                'category',
                EntityType::class,
                array(
                    'class' => "AdminBundle:ArtistCategory",
                    'choice_label' => "name",
                    'attr' => array(
                        'class' => 'form-control'
                    )
                )
            )
            ->add(
                'save',
                SubmitType::class,
                array(
                    'label' => 'Sauvegarder',
                    'attr' => array(
                        'class' => 'btn btn-primary'
                    )
                )
            )
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Hexarec\AdminBundle\Entity\Artist'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'hexarec_adminbundle_artist';
    }


}
