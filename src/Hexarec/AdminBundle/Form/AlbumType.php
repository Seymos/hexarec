<?php

namespace Hexarec\AdminBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AlbumType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'title',
                TextType::class,
                array(
                    'label' => "Titre",
                    'attr' => array(
                        'class' => 'form-control'
                    )
                )
            )
            ->add(
                'slug',
                TextType::class,
                array(
                    'label' => "URL (générée automatiquement)",
                    'attr' => array(
                        'class' => 'form-control disable'
                    )
                )
            )
            ->add(
                'image',
                FileType::class,
                array(
                    'label' => "Cover",
                    'data_class' => null,
                    'required' => false
                )
            )
            ->add(
                'releaseDate',
                DateTimeType::class,
                array(
                    'label' => "Date de sortie",
                    'attr' => array(
                        'class' => 'form-control'
                    )
                )
            )
            ->add(
                'content',
                CKEditorType::class,
                array(
                    'label' => "Contenu",
                    'config' => array(
                        'toolbar' => 'full',
                        'uiColor' => '#ffffff',
                    ),
                )
            )
            ->add(
                'embed',
                TextareaType::class,
                array(
                    'label' => "Player SoundCloud",
                    'attr' => array(
                        'class' => 'form-control'
                    )
                )
            )
            ->add(
                'category',
                EntityType::class,
                array(
                    'class' => "AdminBundle:AlbumCategory",
                    'choice_label' => "name",
                    'label' => "Catégorie de l'album",
                    'attr' => array(
                        'class' => 'form-control'
                    )
                )
            )
            ->add(
                'artist',
                EntityType::class,
                array(
                    'class' => "AdminBundle:Artist",
                    'choice_label' => "name",
                    'label' => "Auteur",
                    'attr' => array(
                        'class' => 'form-control'
                    )
                )
            )
            ->add(
                'save',
                SubmitType::class,
                array(
                    'label' => "Sauvegarder",
                    'attr' => array(
                        'class' => 'form-control'
                    )
                )
            )
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Hexarec\AdminBundle\Entity\Album'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'hexarec_adminbundle_album';
    }


}
