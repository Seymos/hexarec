<?php

namespace Hexarec\AdminBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class SongType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'title',
                TextType::class
            )
            ->add(
                'file',
                FileType::class,
                array(
                    'data_class' => null,
                    'constraints' => [
                        new File([
                            'maxSize' => '20M',
                            'mimeTypes' => [
                                'audio/mpeg',
                            ],
                            'mimeTypesMessage' => 'Please upload a valid Song',
                        ])
                    ]
                )
            )
            ->add(
                'artist',
                EntityType::class,
                array(
                    'class' => "AdminBundle:Artist",
                    'choice_label' => "name",
                    'label' => "Artiste",
                    'attr' => array(
                        'class' => 'form-control'
                    )
                )
            )
            ->add(
                'album',
                EntityType::class,
                array(
                    'class' => "AdminBundle:Album",
                    'choice_label' => "title",
                    'label' => "Album",
                    'attr' => array(
                        'class' => 'form-control'
                    )
                )
            )
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Hexarec\AdminBundle\Entity\Song'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'hexarec_adminbundle_song';
    }


}
