<?php

namespace Hexarec\AdminBundle\Form;

use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ArticleType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'title',
                TextType::class,
                array(
                    'attr' => array(
                        'class' => 'form-control'
                    )
                )
            )
            ->add(
                'date',
                DateTimeType::class,
                array(
                    'attr' => array(
                        'class' => 'form-control'
                    )
                )
            )
            ->add(
                'image',
                FileType::class,
                array(
                    'label' => 'Image',
                    'data_class' => null
                )
            )
            ->add(
                'author',
                EntityType::class,
                array(
                    'class' => "AdminBundle:User",
                    'choice_label' => "username",
                    'attr' => array(
                        'class' => 'form-control'
                    )
                )
            )
            ->add(
                'content',
                CKEditorType::class,
                array(
                    'label' => "Contenu",
                    'config' => array(
                        'toolbar' => 'full',
                        'uiColor' => '#ffffff',
                    ),
                )
            )
            ->add(
                'save',
                SubmitType::class,
                array(
                    'attr' => array(
                        'class' => 'btn btn-primary'
                    )
                )
            )
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Hexarec\AdminBundle\Entity\Article'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'hexarec_adminbundle_article';
    }


}
