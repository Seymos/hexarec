<?php

namespace Hexarec\AdminBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * AlbumCategory
 *
 * @ORM\Table(name="album_category")
 * @ORM\Entity(repositoryClass="Hexarec\AdminBundle\Repository\AlbumCategoryRepository")
 */
class AlbumCategory
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="Album", mappedBy="album_category")
     */
    private $albums;

    public function __construct()
    {
        $this->albums = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return AlbumCategory
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add album
     *
     * @param \Hexarec\AdminBundle\Entity\Album $album
     *
     * @return AlbumCategory
     */
    public function addAlbum(\Hexarec\AdminBundle\Entity\Album $album)
    {
        $this->albums[] = $album;

        return $this;
    }

    /**
     * Remove album
     *
     * @param \Hexarec\AdminBundle\Entity\Album $album
     */
    public function removeAlbum(\Hexarec\AdminBundle\Entity\Album $album)
    {
        $this->albums->removeElement($album);
    }

    /**
     * Get albums
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAlbums()
    {
        return $this->albums;
    }
}
