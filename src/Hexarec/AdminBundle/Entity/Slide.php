<?php

namespace Hexarec\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\JoinColumn;

/**
 * Slide
 *
 * @ORM\Table(name="slide")
 * @ORM\Entity(repositoryClass="Hexarec\AdminBundle\Repository\SlideRepository")
 */
class Slide
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @OneToOne(targetEntity="Media")
     * @JoinColumn(name="media_id", referencedColumnName="id", nullable=true)
     */
    private $media;

    /**
     * @OneToOne(targetEntity="Album")
     * @JoinColumn(name="album_id", referencedColumnName="id", nullable=true)
     */
    private $album;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set media
     *
     * @param \Hexarec\AdminBundle\Entity\Media $media
     *
     * @return Slide
     */
    public function setMedia(\Hexarec\AdminBundle\Entity\Media $media = null)
    {
        $this->media = $media;

        return $this;
    }

    /**
     * Get media
     *
     * @return \Hexarec\AdminBundle\Entity\Media
     */
    public function getMedia()
    {
        return $this->media;
    }

    /**
     * Set album
     *
     * @param \Hexarec\AdminBundle\Entity\Album $album
     *
     * @return Slide
     */
    public function setAlbum(\Hexarec\AdminBundle\Entity\Album $album = null)
    {
        $this->album = $album;

        return $this;
    }

    /**
     * Get album
     *
     * @return \Hexarec\AdminBundle\Entity\Album
     */
    public function getAlbum()
    {
        return $this->album;
    }
}
