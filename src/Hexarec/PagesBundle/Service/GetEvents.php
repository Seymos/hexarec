<?php
namespace Hexarec\PagesBundle\Service;

use Doctrine\ORM\EntityManager;
use Hexarec\AdminBundle\Entity\Event;

class GetEvents
{
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }
    public function getEvents() {
        $em = $this->get('doctrine')->getEntityManager();
        $events = $em->getRepository('AdminBundle:Event')->findAll();

        return $events;
    }
}