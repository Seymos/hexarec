<?php
namespace Hexarec\PagesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class MediaController extends Controller
{

    public function mediaAction($slug) {
        $em = $this->getDoctrine()->getManager();
        $media = $em->getRepository('AdminBundle:Media')->findOneBy(
            array(
                'slug' => $slug
            )
        );

        return $this->render(
            'PagesBundle:Media:media.html.twig',
            array(
                'media' => $media
            )
        );
    }

    public function videosAction() {
        $em = $this->getDoctrine()->getManager();
        $medias = $em->getRepository('AdminBundle:Media')->findAll();
        $categories = $em->getRepository('AdminBundle:MediaCategory')->findAll();

        return $this->render(
            'PagesBundle:Media:videos.html.twig',
            array(
                'medias' => $medias,
                'categories' => $categories
            )
        );
    }
}