<?php

namespace Hexarec\PagesBundle\Controller;

use Hexarec\PagesBundle\Form\ContactForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $bio = $em->getRepository('AdminBundle:Text')->findOneBy(
            array('name' => 'home')
        );
        $slides = $em->getRepository('AdminBundle:Slide')->findAll();
        $nbSlides = count($slides);
        $count = $this->countSlides($nbSlides);

        return $this->render(
            'PagesBundle:Default:index.html.twig',
            array(
                'slides' => $slides,
                'bio' => $bio,
                'count' => $count
            )
        );
    }

    public function setBackgroundAction() {
        $em = $this->getDoctrine()->getManager();
        $background = $em->getRepository('AdminBundle:Background')->findOneBy(
            array('active' => true)
        );
        return $this->render(
            'PagesBundle:Global:setBackground.html.twig',
            array(
                'background' => $background
            )
        );
    }

    public function getEventsAction() {
        $events = $this->get('app.events');
        return $this->render(
            'PagesBundle:Global:events.html.twig',
            array(
                'events' => $events
            )
        );
    }

    /**
     * @param $nbSlides
     * @return int
     */
    public function countSlides($nbSlides) {
        $count = 0;
        if ($nbSlides < 4) {
            $count = $nbSlides;
        } elseif ($nbSlides = 5) {
            $count = $nbSlides - 2;
        } elseif ($nbSlides > 5) {
            $count = round($nbSlides - ($nbSlides/2), 0);
        }
        return $count;
    }

    public function artistsAction()
    {
        $em = $this->getDoctrine()->getManager();
        $artists = $em->getRepository('AdminBundle:Artist')->findBy(
            array(),
            array('name' => 'asc')
        );
        $categories = $em->getRepository('AdminBundle:ArtistCategory')->findAll();
        return $this->render(
            'PagesBundle:Default:artists.html.twig',
            array(
                'artists' => $artists,
                'categories' => $categories
            )
        );
    }

    public function albumsAction()
    {
        $em = $this->getDoctrine()->getManager();
        //$albums = $em->getRepository('AdminBundle:Album')->findAll();
        $albums = $em->getRepository('AdminBundle:Album')->findBy(
            array(),
            array('releaseDate' => 'desc')
        );
        return $this->render(
            'PagesBundle:Default:albums.html.twig',
            array(
                'albums' => $albums
            )
        );
    }

    public function pastEventsAction() {
        $em = $this->getDoctrine()->getManager();
        $today = date("Y-m-d H:i:s");
        $events = $em->getRepository('AdminBundle:Event')->getPastEvents($today);

        return $this->render(
            'PagesBundle:Default:pastEvents.html.twig',
            array(
                'events' => $events
            )
        );
    }

    public function comingEventsAction() {
        $em = $this->getDoctrine()->getManager();
        $today = date("Y-m-d H:i:s");
        $events = $em->getRepository('AdminBundle:Event')->getComingEvents($today);

        return $this->render(
            'PagesBundle:Default:comingEvents.html.twig',
            array(
                'events' => $events
            )
        );
    }

    public function eventsAction() {
        $em = $this->getDoctrine()->getManager();
        $events = $em->getRepository('AdminBundle:Event')->findAll();
        return $this->render(
            'PagesBundle:Default:events.html.twig',
            array(
                'events' => $events
            )
        );
    }

    public function contactAction(Request $request)
    {
        $form = $this->createForm(ContactForm::class);
        $em = $this->getDoctrine()->getManager();
        $partenaires = $em->getRepository('AdminBundle:Partenaire')->findAll();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $message = \Swift_Message::newInstance()
                ->setSubject($form->getData()['sujet'])
                ->setFrom($form->getData()['email'])
                ->setTo('contact@hexarec.com')
                ->setBody(
                    $this->renderView(
                        'PagesBundle:Emails:contact.html.twig',
                        array('name' => $form->getData()['nom'], 'email' => $form->getData()['email'], 'sujet' => $form->getData()['sujet'], 'message' => $form->getData()['message'])
                    ),
                    'text/html'
                )
            ;
            $this->get('mailer')->send($message);
            $this->get('session')->getFlashBag()->add('success','Message sent !');
            return $this->redirectToRoute('contact');
        }

        return $this->render(
            'PagesBundle:Default:contact.html.twig',
            array(
                'form' => $form->createView(),
                'partenaires' => $partenaires
            )
        );
    }

    public function mentionsAction() {
        return $this->render(
            'PagesBundle:Default:mentions.html.twig'
        );
    }
}
