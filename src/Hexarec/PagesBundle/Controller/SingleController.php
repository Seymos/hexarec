<?php
namespace Hexarec\PagesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SingleController extends Controller
{
    public function artistAction($slug) {
        $em = $this->getDoctrine()->getManager();

        $artist = $em->getRepository('AdminBundle:Artist')->findOneBy(
            array(
                'slug' => $slug
            )
        );
        $albums = $artist->getAlbums();

        return $this->render(
            'PagesBundle:Single:artist.html.twig',
            array(
                'artist' => $artist,
                'albums' => $albums
            )
        );
    }

    public function albumAction($slug) {
        $em = $this->getDoctrine()->getManager();

        $album = $em->getRepository('AdminBundle:Album')->findOneBy(
            array(
                'slug' => $slug
            )
        );

        return $this->render(
            'PagesBundle:Single:album.html.twig',
            array(
                'album' => $album
            )
        );
    }

    public function eventAction($slug) {
        $em = $this->getDoctrine()->getManager();

        $event = $em->getRepository('AdminBundle:Event')->findOneBy(
            array(
                'slug' => $slug
            )
        );

        return $this->render(
            'PagesBundle:Single:event.html.twig',
            array(
                'event' => $event
            )
        );
    }
}