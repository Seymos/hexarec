<?php

namespace Hexarec\PagesBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add(
                'nom',
                TextType::class,
                array(
                    'label' => "Votre nom complet",
                    'attr' => array(
                        'placeholder' => "Votre nom complet",
                        'class' => 'form-control'
                    )
                )
            )
            ->add(
                'email',
                EmailType::class,
                array(
                    'label' => "Votre email",
                    'attr' => array(
                        'placeholder' => "Votre email",
                        'class' => 'form-control'
                    )
                )
            )
            ->add(
                'sujet',
                TextType::class,
                array(
                    'label' => "Sujet",
                    'attr' => array(
                        'placeholder' => "Sujet du message",
                        'class' => 'form-control'
                    )
                )
            )
            ->add(
                'message',
                TextareaType::class,
                array(
                    'label' => "Votre message",
                    'attr' => array(
                        'placeholder' => "Votre message",
                        'class' => 'form-control',
                        'rows' => 8
                    )
                )
            )
            ->add(
                'save',
                SubmitType::class,
                array(
                    'label' => "Envoyer",
                    'attr' => array(
                        'class' => "btn btn-primary"
                    )
                )
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => null
        ));
    }
}


class ArticleForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add(
                'title',
                TextType::class,
                array(
                    'label' => "Titre de l'article"
                )
            )
            ->add(
                '$datePublication',
                DateTimeType::class,
                array(
                    'label' => "Date de publication"
                )
            )
            ->add(
                'author',
                TextType::class,
                array(
                    'label' => "Auteur de l'article"
                )
            )
            ->add(
                'slug',
                TextType::class,
                array(
                    'label' => "URL de l'article",
                    'placeholder' => " Format : url-de-l-article"
                )
            )
            ->add(
                'content',
                TextareaType::class,
                array(
                    'label' => "Contenu de l'article"
                )
            )
            ->add(
                'save',
                SubmitType::class,
                array(
                    'label' => "Sauvegarder"
                )
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Admin\AdminBundle\Entity\Articles'
        ));
    }
}