-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Ven 29 Septembre 2017 à 17:38
-- Version du serveur :  10.0.32-MariaDB-0+deb8u1
-- Version de PHP :  5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `hexarec`
--

-- --------------------------------------------------------

--
-- Structure de la table `album`
--

CREATE TABLE `album` (
  `id` int(11) NOT NULL,
  `album_category_id` int(11) DEFAULT NULL,
  `artist_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `release_date` date NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci,
  `embed` longtext COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `album`
--

INSERT INTO `album` (`id`, `album_category_id`, `artist_id`, `title`, `slug`, `release_date`, `content`, `embed`, `image`) VALUES
(1, 1, 1, 'Latenza', 'latenza', '2017-01-01', '<p>Mari Mattham - Latenza</p>', '<iframe width=\"100%\" height=\"450\" scrolling=\"no\" frameborder=\"no\" src=\"https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/299917192&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true\"></iframe>', 'c1afed510d8519b50d0f92e2df4b2ed2.jpeg'),
(2, 2, 1, 'Congo 2.0', 'congo-20', '2016-03-23', '<p>Mari Mattham - Congo 2.0</p>\r\n\r\n<p>incl remix: Echologist</p>', '<iframe width=\"100%\" height=\"450\" scrolling=\"no\" frameborder=\"no\" src=\"https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/208264178&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true\"></iframe>', 'adc4c45e2a50607ef8da2a909d07da55.jpeg'),
(3, 2, 6, 'The broken circle', 'the-broken-circle', '2016-05-18', '<p>Ben Egger - The broken circle</p>\r\n\r\n<p>incl remix: Vakarm</p>', '<iframe width=\"100%\" height=\"450\" scrolling=\"no\" frameborder=\"no\" src=\"https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/220337246&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true\"></iframe>', 'fca300a240dd10a97f13b9a56209730a.jpeg'),
(4, 1, 7, 'Dual Therapy', 'dual-therapy', '2016-01-13', '<p>Pleije - Dual Therapy</p>', '<iframe width=\"100%\" height=\"450\" scrolling=\"no\" frameborder=\"no\" src=\"https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/173330352&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true\"></iframe>', 'acd6eefc77f2cde74d58219af9e92755.jpeg'),
(5, 2, 2, 'Spartacus', 'spartacus', '2015-09-02', '<p>DJ Saint Pierre - Spartacus</p>\r\n\r\n<p>incl remixes: DJ Hi-Shock / D&#39;jamency / M23 / Ben Egger</p>', '<iframe width=\"100%\" height=\"450\" scrolling=\"no\" frameborder=\"no\" src=\"https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/136219632&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true\"></iframe>', '554223b7b7316126420627500a200bad.jpeg'),
(6, 2, 2, 'Plage', 'plage', '2014-04-30', '<p>DJ Saint Pierre - Plage</p>\r\n\r\n<p>incl remixes: Scan X / Stcl3m / Zero Order</p>', '<iframe width=\"100%\" height=\"450\" scrolling=\"no\" frameborder=\"no\" src=\"https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/22412626&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true\"></iframe>', '82b994fa7a7e4c89a23a8e43b00a3ffe.jpeg'),
(7, 1, 9, 'Four Seasons', 'four-seasons', '2015-03-04', '<p>M23 - Four Seasons</p>', '<iframe width=\"100%\" height=\"450\" scrolling=\"no\" frameborder=\"no\" src=\"https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/86250026&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true\"></iframe>', '95b14e252e18c69e633dba5be823bbc2.jpeg'),
(8, 1, 9, 'Four Seasons (the remixes)', 'four-seasons-the-remixes', '2015-05-13', '<p>M23 - Four Seasons (the remixes)</p>\r\n\r\n<p>incl remixes: Scan X / IORI / DJ Saint Pierre / Klinika / Keira / Dykore / Andrea Atzeni</p>', '<iframe width=\"100%\" height=\"450\" scrolling=\"no\" frameborder=\"no\" src=\"https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/102856807&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true\"></iframe>', '24670bfc59aedb92eb140d7ccec90028.jpeg'),
(9, 2, 2, 'N\'aie pas peur', 'naie-pas-peur', '2014-06-19', '<p>DJ Saint Pierre - N&#39;aie pas peur</p>\r\n\r\n<p>incl remix: Stcl3m</p>', '<iframe width=\"100%\" height=\"450\" scrolling=\"no\" frameborder=\"no\" src=\"https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/38902935&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true\"></iframe>', '7f0a82457c213f4a14c35979407d1342.jpeg'),
(10, 2, 2, 'Germinal', 'germinal', '2014-09-28', '<p>DJ Saint Pierre - Germinal</p>\r\n\r\n<p>incl remix: Andre Sobota</p>', '<iframe width=\"100%\" height=\"450\" scrolling=\"no\" frameborder=\"no\" src=\"https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/46716908&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true\"></iframe>', 'c16df51f2d0a94582b3bed2cfeaf117c.jpeg'),
(11, 2, 2, 'Sonotherapie', 'sonotherapie', '2015-01-15', '<p>DJ Saint Pierre - sonotherapie</p>\r\n\r\n<p>incl remix: Kianga</p>', '<iframe width=\"100%\" height=\"450\" scrolling=\"no\" frameborder=\"no\" src=\"https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/72398525&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true\"></iframe>', '9bd38db8734879edec4dddb8a08ccbf7.jpeg'),
(12, 2, 2, 'Saens', 'saens', '2017-02-01', '<p>DJ Saint Pierre - Saens</p>\r\n\r\n<p>COMING SOON</p>', '<iframe width=\"100%\" height=\"450\" scrolling=\"no\" frameborder=\"no\" src=\"https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/300603564%3Fsecret_token%3Ds-grsDa&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true\"></iframe>', 'fc1d04bb8356658f8be5c25d6d629862.jpeg'),
(14, 1, 10, 'Xhe-3 aka Tom', 'xhe-3-aka-tom', '2017-04-19', '<p>55h22 - Xhe-3 aka Tom Xhe-3 aka Tom is a compilation of 4 tracks that were performed live on a modular synthesizer. 55h22 tells here the story of Tom, a young human being made of flesh and bones, manipulated by some post-digital/immaterial entity.</p>\r\n\r\n<p>1) Improving Species</p>\r\n\r\n<p>2)Escape</p>\r\n\r\n<p>3)Chase</p>\r\n\r\n<p>4)Suffocation</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>HEXA REC 019</p>', '<iframe width=\"100%\" height=\"450\" scrolling=\"no\" frameborder=\"no\" src=\"https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/318298507&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true\"></iframe>', 'da9521b6ca02aa9f144975d49d088de9.jpeg'),
(15, 2, 11, 'Drongar', 'drongar', '2017-05-31', '<div class=\"sc-type-small\">\r\n<div>\r\n<p>Hexa Rec 020</p>\r\n\r\n<p>Tuchy Frunk - Drongar<br />\r\nTuchy Frunk - Tsoss Beacon<br />\r\nTuchy Frunk - Void Station</p>\r\n</div>\r\n</div>', '<iframe width=\"100%\" height=\"450\" scrolling=\"no\" frameborder=\"no\" src=\"https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/325347654&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true\"></iframe>', 'eb604db16c82000858da6dc96b0049f2.jpeg'),
(16, 2, 12, 'Geist', 'geist', '2017-06-14', '<p>Hexa Rec 021</p>\r\n\r\n<p>Nim&auml; Skill - Geist</p>\r\n\r\n<p>Nim&auml; Skill - Obscur Nebulae</p>\r\n\r\n<p>Nim&auml; Skill - Solar Parallax</p>', '<iframe width=\"100%\" height=\"450\" scrolling=\"no\" frameborder=\"no\" src=\"https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/327868672&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true\"></iframe>', '1115a4f8215f49833f1022112ef4ba8b.jpeg'),
(17, 1, 13, 'Expect the damage', 'expect-the-damage', '2017-08-02', '<p>Hexa Rec 022</p>\r\n\r\n<p>Dawn Razor - Expect the damage</p>\r\n\r\n<p>Dawn Razor - Not your story</p>', 'https://soundcloud.com/hexarec/dawn-razor-expect-the-damage-hexarec022', 'bc1b0cfda3f6fc30dbb1d8b3e3e843d9.jpeg');

-- --------------------------------------------------------

--
-- Structure de la table `album2`
--

CREATE TABLE `album2` (
  `id` int(11) NOT NULL,
  `album_category_id` int(11) DEFAULT NULL,
  `artist_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `release_date` date NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `embed` longtext COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `album_artists`
--

CREATE TABLE `album_artists` (
  `album_id` int(11) NOT NULL,
  `artist_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `album_category`
--

CREATE TABLE `album_category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `album_category`
--

INSERT INTO `album_category` (`id`, `name`) VALUES
(1, 'Album'),
(2, 'EP');

-- --------------------------------------------------------

--
-- Structure de la table `article`
--

CREATE TABLE `article` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `artist`
--

CREATE TABLE `artist` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci,
  `slug` longtext COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `artist`
--

INSERT INTO `artist` (`id`, `category_id`, `name`, `content`, `slug`, `image`, `facebook`) VALUES
(1, 1, 'Mari Mattham', '<p>Mari Mattham</p>', 'mari-mattham', '0fb7c4abcd08e1f5a062547f78a7374a.jpeg', NULL),
(2, 1, 'DJ Saint Pierre', '<p>DJ Saint Pierre</p>', 'dj-saint-pierre', '72654468a73f1661b062e53cca48352c.jpeg', 'https://www.facebook.com/saintpierredj/'),
(3, 2, 'Iori', '<p>IORI</p>', 'iori', '065e1cbba44e58cfc73a93f589f0d651.jpeg', NULL),
(4, 2, 'Scan X', '<p>Scan X</p>', 'scan-x', 'c4e14360c6f6a054f28585ac5b1617e2.jpeg', NULL),
(5, 2, 'DJ Hi-Shock', '<p>DJ Hi-Shock</p>', 'dj-hi-shock', '9f360cdf11536d90144c5c35fc750ee2.jpeg', NULL),
(6, 1, 'Ben Egger', '<p>Ben Egger</p>', 'ben-egger', '901039547b3435f5a3af86d4c57a649f.jpeg', NULL),
(7, 2, 'Pleije', '<p>Pleije</p>', 'pleije', 'ac030d630603d76ac00a0e294cdad661.jpeg', NULL),
(9, 1, 'M23', '<p>M23</p>', 'm23', '00fe3fb9437b9de3eb1da301e4d5aaf6.jpeg', NULL),
(10, 1, '55H22', '<p>55H22</p>\r\n\r\n<p>Techno avec concession dont plein de gens ont le secret.</p>\r\n\r\n<p><a href=\"https://www.facebook.com/55h22/\"><img alt=\"\" src=\"https://ucarecdn.com/ff7fa0bd-f586-4dea-8fb9-cf1e0caf2ab6/-/crop/265x265/68,68/-/preview/\" style=\"height:44px; width:44px\" /></a></p>', '55h22', '3fc97d497328aa479e9684be6b71f409.jpeg', NULL),
(11, 1, 'TUCHY Frunk', '<p>Very curious and reserved, Tuchy Frunk gives way through his techno productions he defines as raw and contrasted. He represents a crossroad to a universe where techno is much more personal than deeply rooted in time.</p>', 'tuchy-frunk', '170ffa9a4886c43c8aba260b8f3c7c54.jpeg', 'https://www.facebook.com/tuchyf/?fref=ts'),
(12, 1, 'Nima Skill', '<p>Turntables, Vinyl and Passion since 1994</p>', 'nima-skill', 'aa552af9c4f09558b9dd4451cab7b5b1.jpeg', 'https://www.facebook.com/NimaSkillDJ/?fref=ts'),
(13, 1, 'Dawn Razor', '<p>Enigmatic Russian techno-head, Dawn Razor, is on hand to brighten things up with a brand new alias. We only know two things about this producer; firstly, according to the info on his official page, he&#39;s &quot;exploring the abandoned world of techno,&quot; and secondly, he&#39;s a damn fine explorer! His first 3 tracker EP &#39;A-Tention&#39; is up to be released on Italian Intellighenzia Electronica records with 3 originals onboard plus a great remix from techno heavyweight Dubspeeka. Also more news to come nearest days from Dawn Razor but for now let speculation commence over the identity of this rising artist!</p>', 'dawn-razor', '693be6df4c036056fb782b7eec1b61a9.jpeg', 'https://www.facebook.com/awnazor/?fref=ts');

-- --------------------------------------------------------

--
-- Structure de la table `artists_songs`
--

CREATE TABLE `artists_songs` (
  `artist_id` int(11) NOT NULL,
  `song_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `artist_category`
--

CREATE TABLE `artist_category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `artist_category`
--

INSERT INTO `artist_category` (`id`, `name`, `slug`) VALUES
(1, 'hexa artist', 'hexa-artist'),
(2, 'Remixer / Other', 'remixer-other');

-- --------------------------------------------------------

--
-- Structure de la table `background`
--

CREATE TABLE `background` (
  `id` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `background`
--

INSERT INTO `background` (`id`, `image`, `active`) VALUES
(1, '3132c63b7a07f4230200d00de933d302.jpeg', 1);

-- --------------------------------------------------------

--
-- Structure de la table `event`
--

CREATE TABLE `event` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `slug` longtext COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `media`
--

CREATE TABLE `media` (
  `id` int(11) NOT NULL,
  `media_category_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci,
  `url` longtext COLLATE utf8_unicode_ci,
  `code` longtext COLLATE utf8_unicode_ci,
  `datePublication` date DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `media`
--

INSERT INTO `media` (`id`, `media_category_id`, `title`, `slug`, `content`, `url`, `code`, `datePublication`, `image`) VALUES
(1, 1, 'Hexa Rec // Le Podcast 01: DJ Saint Pierre', 'hexa-rec-le-podcast-01-dj-saint-pierre', '<h1><span dir=\"ltr\">Hexa Rec // Le Podcast 01: DJ Saint Pierre </span></h1>', NULL, '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/oV7qaxBP-ao\" frameborder=\"0\" allowfullscreen></iframe>', NULL, NULL),
(2, 1, 'Hexa Rec // Le Podcast 02: Rochel', 'hexa-rec-le-podcast-02-rochel', '<h1><span dir=\"ltr\">Hexa Rec // Le Podcast 02: Rochel </span></h1>', NULL, '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/TQlVRRE1EFY\" frameborder=\"0\" allowfullscreen></iframe>', NULL, NULL),
(3, 1, 'Hexa Rec // Le Podcast 03: Ben Egger', 'hexa-rec-le-podcast-03-ben-egger', '<h1><span dir=\"ltr\">Hexa Rec // Le Podcast 03: Ben Egger </span></h1>', NULL, '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/AGBGGXzWauE\" frameborder=\"0\" allowfullscreen></iframe>', NULL, NULL),
(4, 1, 'Hexa Rec // Le Podcast 04: Ricardo Garduno', 'hexa-rec-le-podcast-04-ricardo-garduno', '<h1><span dir=\"ltr\">Hexa Rec // Le Podcast 04: Ricardo Garduno </span></h1>', NULL, '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/elKzjDrkSn0\" frameborder=\"0\" allowfullscreen></iframe>', NULL, 'ded073f311734d541a56a487d16fec24.jpeg'),
(5, 2, 'DJ Saint Pierre - Spartacus (video teaser)', 'dj-saint-pierre-spartacus-video-teaser', '<p>DJ Saint Pierre - Spartacus (video teaser)</p>', NULL, '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/w-ZENbqac4M\" frameborder=\"0\" allowfullscreen></iframe>', NULL, NULL),
(6, 2, 'M23-Four Seasons (The remixes)', 'm23-four-seasons-the-remixes', '<p>M23-Four Seasons (The remixes) (video teaser)</p>', NULL, '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/tPjmAFKT7o0\" frameborder=\"0\" allowfullscreen></iframe>', NULL, NULL),
(7, 3, 'Hexa Rec Label night @ Kazkabar', 'hexa-rec-label-night-kazkabar', '<p>Hexa Rec Label night @ Kazkabar</p>', NULL, '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/S02TG17LXeM\" frameborder=\"0\" allowfullscreen></iframe>', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `media_category`
--

CREATE TABLE `media_category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `media_category`
--

INSERT INTO `media_category` (`id`, `name`, `slug`) VALUES
(1, 'Podcast', 'podcast'),
(2, 'Clip', 'clip'),
(3, 'Other', 'other');

-- --------------------------------------------------------

--
-- Structure de la table `partenaire`
--

CREATE TABLE `partenaire` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `partenaire`
--

INSERT INTO `partenaire` (`id`, `name`, `slug`, `url`, `image`) VALUES
(1, 'Centre culturel LE BOURNOT', 'centre-culturel-le-bournot', 'http://www.sallelebournot.fr/', 'f741d3b2e8f08318ef708e8be9cf6c17.png'),
(2, 'The Underground', 'the-underground', 'http://www.theunderground.fr/', 'ddd05a93d0cc5e77985689e16e9346da.jpeg');

-- --------------------------------------------------------

--
-- Structure de la table `slide`
--

CREATE TABLE `slide` (
  `id` int(11) NOT NULL,
  `album_id` int(11) DEFAULT NULL,
  `media_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `slide`
--

INSERT INTO `slide` (`id`, `album_id`, `media_id`) VALUES
(32, 17, NULL),
(33, 16, NULL),
(34, 15, NULL),
(35, NULL, 4);

-- --------------------------------------------------------

--
-- Structure de la table `song`
--

CREATE TABLE `song` (
  `id` int(11) NOT NULL,
  `artist_id` int(11) DEFAULT NULL,
  `album_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `text`
--

CREATE TABLE `text` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `text`
--

INSERT INTO `text` (`id`, `name`, `content`) VALUES
(1, 'home', '<p>Founded in 2014 by two artists from Lyon, Hexa Rec is an electronic music label with a tendency for eclectism. It&#39;s catalogue is tinged with Techno, Break Beat or even experimental influences. Send us you demos by private SoundCloud link at <a href=\"mailto:hexarec@gmail.com\">hexarec@gmail.com</a>.</p>\n\n<p>&nbsp;</p>\n\n<p>Fond&eacute; en 2014 par deux artistes Lyonnais, Hexa Rec est un label plut&ocirc;t &eacute;clectique de musique &eacute;lectronique aux tendances Techno. Hexarec dispose d&#39;un catalogue assez vari&eacute; passant tour &agrave; tour d&#39;une Techno fa&ccedil;onn&eacute;e pour le dance floor &agrave; des breakbeat aux influences exp&eacute;rimentales. L&#39;histoire du label d&eacute;bute en Mai 2014 par la sortie de l&#39;EP &quot;Plage&quot; de DJ Saint Pierre notamment remix&eacute; par Scan X.</p>\n\n<p>&nbsp;</p>\n\n<p>Hexa Rec est avant tout un label associatif et familial donc le souhait et de rester libre sur son choix artistique, permettant &agrave; la fois de produire sans complexes de jeunes talents encore inconnus mais aussi des artistes de renomm&eacute;es internationales (Scan X, Andr&eacute; Sobota, Cosmic Boys, M23).</p>\n\n<p>&nbsp;</p>\n\n<p>Pour toutes d&eacute;mos, merci de nous faire parvenir vos liens priv&eacute;s SoundCloud &agrave; l&#39;adresse <a href=\"mailto:hexarec@gmail.com\">hexarec@gmail.com</a>.</p>');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `locked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `confirmation_token` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `credentials_expire_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `locked`, `expires_at`, `confirmation_token`, `password_requested_at`, `roles`, `credentials_expire_at`) VALUES
(3, 'hexarec', 'hexarec', 'hexarec@gmail.com', 'hexarec@gmail.com', 1, '70foeuihcm0w8g4s4s84s0g4sgc008w', '$2y$13$1vzYnOmoDKxa9wst1ON9HOr.3aU3qpOcPiw5aiWnaCtWZ95zL.2nW', '2017-07-26 16:25:53', 0, NULL, NULL, NULL, 'a:1:{i:0;s:10:\"ROLE_ADMIN\";}', NULL),
(4, 'seymos', 'seymos', 'contact@louisthomas.fr', 'contact@louisthomas.fr', 1, '9lydbupwycws4sw04oko8wcw440s0sk', '$2y$13$k00or2PUh19Frq/G.xCJau97Qpr7dT1.4.z3Qg7wsLLhAr84iZwKy', '2017-04-18 18:36:43', 0, NULL, NULL, NULL, 'a:2:{i:0;s:10:\"ROLE_ADMIN\";i:1;s:16:\"ROLE_SUPER_ADMIN\";}', NULL);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `album`
--
ALTER TABLE `album`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_39986E43594A49B6` (`album_category_id`),
  ADD KEY `IDX_39986E43B7970CF8` (`artist_id`);

--
-- Index pour la table `album2`
--
ALTER TABLE `album2`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_F5393649594A49B6` (`album_category_id`),
  ADD KEY `IDX_F5393649B7970CF8` (`artist_id`);

--
-- Index pour la table `album_artists`
--
ALTER TABLE `album_artists`
  ADD PRIMARY KEY (`album_id`,`artist_id`),
  ADD UNIQUE KEY `UNIQ_3D04DD0CB7970CF8` (`artist_id`),
  ADD KEY `IDX_3D04DD0C1137ABCF` (`album_id`);

--
-- Index pour la table `album_category`
--
ALTER TABLE `album_category`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_23A0E66A76ED395` (`user_id`);

--
-- Index pour la table `artist`
--
ALTER TABLE `artist`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_159968712469DE2` (`category_id`);

--
-- Index pour la table `artists_songs`
--
ALTER TABLE `artists_songs`
  ADD PRIMARY KEY (`artist_id`,`song_id`),
  ADD UNIQUE KEY `UNIQ_CACB0291A0BDB2F3` (`song_id`),
  ADD KEY `IDX_CACB0291B7970CF8` (`artist_id`);

--
-- Index pour la table `artist_category`
--
ALTER TABLE `artist_category`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `background`
--
ALTER TABLE `background`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_6A2CA10CE52EEF71` (`media_category_id`);

--
-- Index pour la table `media_category`
--
ALTER TABLE `media_category`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `partenaire`
--
ALTER TABLE `partenaire`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `slide`
--
ALTER TABLE `slide`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_72EFEE621137ABCF` (`album_id`),
  ADD UNIQUE KEY `UNIQ_72EFEE62EA9FDD75` (`media_id`);

--
-- Index pour la table `song`
--
ALTER TABLE `song`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_33EDEEA1B7970CF8` (`artist_id`),
  ADD UNIQUE KEY `UNIQ_33EDEEA11137ABCF` (`album_id`);

--
-- Index pour la table `text`
--
ALTER TABLE `text`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8D93D64992FC23A8` (`username_canonical`),
  ADD UNIQUE KEY `UNIQ_8D93D649A0D96FBF` (`email_canonical`),
  ADD UNIQUE KEY `UNIQ_8D93D649C05FB297` (`confirmation_token`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `album`
--
ALTER TABLE `album`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT pour la table `album2`
--
ALTER TABLE `album2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `album_category`
--
ALTER TABLE `album_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `article`
--
ALTER TABLE `article`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `artist`
--
ALTER TABLE `artist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT pour la table `artist_category`
--
ALTER TABLE `artist_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `background`
--
ALTER TABLE `background`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `event`
--
ALTER TABLE `event`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `media`
--
ALTER TABLE `media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT pour la table `media_category`
--
ALTER TABLE `media_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `partenaire`
--
ALTER TABLE `partenaire`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `slide`
--
ALTER TABLE `slide`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT pour la table `song`
--
ALTER TABLE `song`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `text`
--
ALTER TABLE `text`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `album`
--
ALTER TABLE `album`
  ADD CONSTRAINT `FK_39986E43594A49B6` FOREIGN KEY (`album_category_id`) REFERENCES `album_category` (`id`),
  ADD CONSTRAINT `FK_39986E43B7970CF8` FOREIGN KEY (`artist_id`) REFERENCES `artist` (`id`);

--
-- Contraintes pour la table `album2`
--
ALTER TABLE `album2`
  ADD CONSTRAINT `FK_F5393649594A49B6` FOREIGN KEY (`album_category_id`) REFERENCES `album_category` (`id`),
  ADD CONSTRAINT `FK_F5393649B7970CF8` FOREIGN KEY (`artist_id`) REFERENCES `artist` (`id`);

--
-- Contraintes pour la table `album_artists`
--
ALTER TABLE `album_artists`
  ADD CONSTRAINT `FK_3D04DD0C1137ABCF` FOREIGN KEY (`album_id`) REFERENCES `album` (`id`),
  ADD CONSTRAINT `FK_3D04DD0CB7970CF8` FOREIGN KEY (`artist_id`) REFERENCES `artist` (`id`);

--
-- Contraintes pour la table `article`
--
ALTER TABLE `article`
  ADD CONSTRAINT `FK_23A0E66A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `artist`
--
ALTER TABLE `artist`
  ADD CONSTRAINT `FK_159968712469DE2` FOREIGN KEY (`category_id`) REFERENCES `artist_category` (`id`);

--
-- Contraintes pour la table `artists_songs`
--
ALTER TABLE `artists_songs`
  ADD CONSTRAINT `FK_CACB0291A0BDB2F3` FOREIGN KEY (`song_id`) REFERENCES `song` (`id`),
  ADD CONSTRAINT `FK_CACB0291B7970CF8` FOREIGN KEY (`artist_id`) REFERENCES `artist` (`id`);

--
-- Contraintes pour la table `media`
--
ALTER TABLE `media`
  ADD CONSTRAINT `FK_6A2CA10CE52EEF71` FOREIGN KEY (`media_category_id`) REFERENCES `media_category` (`id`);

--
-- Contraintes pour la table `slide`
--
ALTER TABLE `slide`
  ADD CONSTRAINT `FK_72EFEE621137ABCF` FOREIGN KEY (`album_id`) REFERENCES `album` (`id`),
  ADD CONSTRAINT `FK_72EFEE62EA9FDD75` FOREIGN KEY (`media_id`) REFERENCES `media` (`id`);

--
-- Contraintes pour la table `song`
--
ALTER TABLE `song`
  ADD CONSTRAINT `FK_33EDEEA11137ABCF` FOREIGN KEY (`album_id`) REFERENCES `album` (`id`),
  ADD CONSTRAINT `FK_33EDEEA1B7970CF8` FOREIGN KEY (`artist_id`) REFERENCES `artist` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
